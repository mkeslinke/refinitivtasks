package excersises.taskA1;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;


public class HTMLParser {

    private static int DATE_INDEX = 0;
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy/MM/dd").withZoneUTC();
    private static final DateTimeFormatter SECOND_DATE_FORMAT = DateTimeFormat.forPattern("ddMMyyyy").withZoneUTC();
    private static final DateTimeFormatter THIRD_DATE_FORMAT = DateTimeFormat.forPattern("dd-MM-yyyy").withZoneUTC();

    public static void main(String[] args) throws IOException {
        Document document = parseHtml("test-quiz-variant-B/task_A1.html");
        if (document == null)
            throw new RuntimeException("File not accesable.");

        String date;
        Elements rows = getTableElement(document, "tr");
        for (Element row : rows) {
            date = getDate(row);
            String valueRow = getValueRow(row);
            valueRow = changeFormat(valueRow);
            System.out.print(date);
            System.out.println(valueRow);
        }
    }

    private static String getDate(Element row) {
        Element dateCell = row.getElementsByTag("th").get(DATE_INDEX);
        String stringCell = dateCell.text().replaceAll("T0000", "");
        DateTime valueDate = null;
        try {
            valueDate = DATE_FORMAT.parseDateTime(stringCell);
        } catch (IllegalArgumentException exception) {
            try {
                valueDate = SECOND_DATE_FORMAT.parseDateTime(stringCell);
            } catch (IllegalArgumentException exception2) {
                valueDate = THIRD_DATE_FORMAT.parseDateTime(stringCell);
            }
        }
        return valueDate.toString("yyyy-MM-dd");
    }

    private static String getValueRow(Element row) {
        String rowString = "";
        Elements cells = row.getElementsByTag("th");
        cells.remove(DATE_INDEX);

        for (Element cell : cells) {
            String cellString = cell.text().toUpperCase();
            rowString += " " + cellString;

        }
        return rowString;
    }

    private static Document parseHtml(String path) {
        File file = new File(path);
        Document document = null;
        try {
            document = Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    private static Elements getTableElement(Document document, String string) {
        return document.getElementsByTag(string);
    }

    private static String changeFormat(String stringRow) {
        String string = stringRow.replaceAll("(?<=\\d)(\\.)(?=\\d)", ",")
                .replaceAll("(?<=\\d)(,)(?=\\d{3})", "")
                .replaceAll("%", " ")
                .replaceAll("\\+", "Y")
                .replaceAll(" *\\(\\d\\)", "")
                .replaceAll(" *\\(\\d\\)", "")
                .replaceAll(" *\\(\\d\\)", "")
                .replaceAll(" *\\.", " ");
     return string;
    }
}






