package excersises.taskA4;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class XLSScraper {

    private static final String XLSFILEPATH = "Desequilibre_Charges_2017-10-15_2017-11-14.xls";

    public static void main(String[] args) throws IOException, InvalidFormatException {

        Workbook workbook = WorkbookFactory.create(new File(XLSFILEPATH));
        Sheet northSheet = workbook.getSheetAt(1);
        List<String> dateList = new ArrayList<>();
        List<Double> alizesList = new ArrayList<>();
        List<Double> kWhList = new ArrayList<>();
        String startDate = "16/10/2017";
        String endDate = "30/10/2017";


        getDates(northSheet, dateList);
        getAlizesValues(northSheet, dateList, startDate, endDate, alizesList);
        getkWhValues(northSheet, dateList, startDate, endDate, kWhList);


        System.out.println("Net imbalances through the ALIZES service: "
                + startDate + " - " + endDate + " SUM: " + getSum(kWhList) + "  AVG: " + getAvg(kWhList) +
                "\n Imbalances cashed out through sells at marginal price (kWh 25°C): "
                + startDate + " - " + endDate + ": SUM: " + getSum(alizesList) + ", AVG: " + getAvg(alizesList));
    }

    private static Double getAvg(List<Double> doubleList) {
        return getSum(doubleList) / doubleList.size();
    }

    private static Double getSum(List<Double> doubleList) {
        double sum = 0.0;
        for (Double current : doubleList) {
            sum += current;
        }
        return sum;
    }

    private static void getkWhValues(Sheet northSheet, List<String> dateList, String startDate, String endDate, List<Double> kWhList) {
        DataFormatter dataFormatter = new DataFormatter();
        int length = getDateIndex(endDate, dateList);
        int startLoop = getDateIndex(startDate, dateList);

        for (int i = 4 + startLoop; i < length + 5; i++) {
            String val = dataFormatter.formatCellValue(northSheet.getRow(i).getCell(1)).replaceAll("[^\\d]", "");
            String val2 = dataFormatter.formatCellValue(northSheet.getRow(i).getCell(5)).replaceAll("[^\\d]", "");

            kWhList.add(Double.parseDouble(val));
            kWhList.add(Double.parseDouble(val2));
        }
    }

    private static void getAlizesValues(Sheet northSheet, List<String> dateList, String startDate, String endDate, List<Double> alizesList) {
        DataFormatter dataFormatter = new DataFormatter();
        int startLoop = getDateIndex(startDate, dateList);
        int length = getDateIndex(endDate, dateList);


        for (int i = 4 + startLoop; i < length + 5; i++) {
            String val = dataFormatter.formatCellValue(northSheet.getRow(i).getCell(2)).replaceAll("[^\\d]", "");
            String val2 = dataFormatter.formatCellValue(northSheet.getRow(i).getCell(6)).replaceAll("[^\\d]", "");

            alizesList.add(Double.parseDouble(val));
            alizesList.add(Double.parseDouble(val2));
        }
    }

    private static int getDateIndex(String startDate, List<String> dateList) {

        int index = 0;
        for (int i = 0; i < dateList.size(); i++) {
            if (startDate.equals(dateList.get(i))) {
                index = i;
            }
        }
        return index;
    }

    private static void getDates(Sheet northSheet, List<String> dateList) {
        DataFormatter dataFormatter = new DataFormatter();

        int length = northSheet.getLastRowNum();
        for (int i = 4; i < length + 1; i++) {
            String val = dataFormatter.formatCellValue(northSheet.getRow(i).getCell(0));
            dateList.add(val);
        }
    }


}


