package excersises.taskA5;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HurricaneDataFinder {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("test-quiz-variant-B/file.txt");
        getHurricane(path);
    }

    public static void getHurricane(Path path) throws IOException {
        Scanner scanner = new Scanner(path);
        String hurricaneID = null;
        String hurricaneName = null;
        int hurricaneSpeed = 0;

        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            Pattern hurricaneDataPattern = Pattern.compile("(^[EP]{2}\\d{2}2016,.*A,).{5,6}\\d");
            Matcher hurricaneDataMatcher = hurricaneDataPattern.matcher(currentLine);

            if (hurricaneDataMatcher.find()) {
                String[] hurricaneData = hurricaneDataMatcher.group(0).split(",");
                hurricaneID = hurricaneData[0].trim();
                hurricaneName = hurricaneData[1].trim();
                hurricaneSpeed = Integer.valueOf(hurricaneData[2].trim());

                System.out.println("Hurricane: " + hurricaneID + "-" + hurricaneName + ", its maximum sustained wind speed is " +
                        + hurricaneSpeed + " knots.");
            }
        }
    }
}


