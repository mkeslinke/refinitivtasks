package excersises.taskB1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class TaskB1 {

    // Two solutions - 1) takes html table and store it to another html file
    public static void main(String[] args) throws IOException {
        Document document = Jsoup.connect("https://www.ote-cr.cz/en/statistics/electricity-imbalances-1").get();
        String tables = String.valueOf(document.getElementsByTag("table"));

        File htmlFile = new File("taskB1.html");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(htmlFile));
            bufferedWriter.write("<html><body><div>");
            bufferedWriter.write(tables);
            bufferedWriter.write("</div></body></html>");
            bufferedWriter.close();
        }catch (IOException e ){
            e.printStackTrace();
        }

    }
}
