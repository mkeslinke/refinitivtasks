package excersises.taskB1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TaskB1Second {

    /// Different solution - 2) takes data and stores it to html

    public static void main(String[] args) throws IOException {

        List<String> rowList = new ArrayList<String>();

        Document document = Jsoup.connect("https://www.ote-cr.cz/en/statistics/electricity-imbalances-1").get();
        Elements tables = document.select("table");
        String caption = String.valueOf(document.getElementsByTag("caption").text());

        for (Element table : tables) {
            getRows(rowList, table);
        }

        File htmlFile = new File("taskB1Second.html");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(htmlFile));
            bufferedWriter.write(caption + "\n");
            for (String row : rowList) {
                bufferedWriter.write(row);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List getRows(List<String> rowList, Element table) {
        Elements cells = table.getElementsByTag("tr");
        for (Element cell : cells) {
            rowList.add(cell.text().trim());

        }
        return rowList;
    }

}
