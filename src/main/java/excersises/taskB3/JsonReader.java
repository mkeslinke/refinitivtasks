package excersises.taskB3;

import javax.json.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class JsonReader {

    public static void main(String a[]) {

        File jsonInputFile = new File("test-quiz-variant-B/task_B3valid.json");
        InputStream is;
        try {
            is = new FileInputStream(jsonInputFile);
            javax.json.JsonReader reader = Json.createReader(is);
            JsonArray arrObj = reader.readArray();
            reader.close();
            for (int i = 0; i < arrObj.size(); i++) {

                JsonObject item = arrObj.getJsonObject(i);
                String name = item.getString("name");
                String priceUsd = item.getString("price_usd");
                System.out.println(name +" : " + priceUsd);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
